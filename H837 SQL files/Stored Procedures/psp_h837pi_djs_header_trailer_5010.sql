IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES 
		WHERE ROUTINE_NAME = 'psp_h837pi_djs_header_trailer_5010'
		AND ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_SCHEMA = 'dbo'
	   )
	DROP PROCEDURE dbo.psp_h837pi_djs_header_trailer_5010
go

CREATE proc dbo.psp_h837pi_djs_header_trailer_5010
@sp_id int = 0
as
/*******************************************************************
 Stored Procedure: dbo.psp_h837pi_djs_header_trailer_5010               
 Creation Date   : 10.22.2002                                      
 Copyright       : 2002 Askesis Development Group, Inc.            
                                                                   
 Purpose: Create the source SQL to DataJunction for 837 processing.
                                                                   
 Output Parameters:                                                
                                                                   
 Return Status:                                                    
                                                                   
 Called By:  ???						     
                                                                   
 Calls:                                                            
                                                                   
 Data Modifications:                                               
                                                                   
 Updates:                                                          
   Date     Author      Purpose                                    
   10.22.02 Raghu       Created.                                   
   12.30.04 BPillai	  #16042, Fixed the for performance
   11/30/2010 NLEY	  #21752 - Coded for 5010 implementation.
   12/13/2010 NLEY        #21752 - Removed code used during testing.	
   01/10/2011 NLEY        #21752/21146 - Copied from original script, psp_h837pi_djs_header_trailer. Cosmos Maps needs 
					 seperate scripts for 4010 versus 5010.     
   02.05.13 Sri		25997:Corrected deprecated raiseerror syntax
*******************************************************************/
----------------------------------
-- Declare the Variables
----------------------------------

declare @errmsg		varchar(255) 
	 /*  Begin 01/10/2011 Change,
	@interchange_control_version_no		char(5),  -- #21146/#21752/#21934/#21936
	@version_code				varchar(25)  -- #21146/#21752/#21934/#21936
End  01/10/2011 Change
*/

----------------------------------
-- Core Logic
----------------------------------
/*  Begin 01/10/2011 Change
-- #temp_arguments is no longer available during the djs process, so use psfn_h837pi_djs_version_code.
select @version_code = dbo.psfn_h837pi_djs_version_code(@sp_id)                           -- #21146/#21752/#21934/#21936
	, @interchange_control_version_no = left(dbo.psfn_h837pi_djs_version_code(@sp_id),5)  -- #21146/#21752/#21934/#21936

IF @interchange_control_version_no = '00501' BEGIN

End 01/10/2011 Change
*/

----------------------------------
-- this is the 5010 code
----------------------------------
select ISA_01_I01=a.authorization_id_qualifier, ISA_02_I02=a.authorization_id, ISA_03_I03=a.security_id_qualifier, 
ISA_04_I04=a.security_id, ISA_05_I05=a.interchange_sender_qualifier, ISA_06_I06=a.interchange_sender_id, 
ISA_07_I05=a.interchange_receiver_qualifier, ISA_08_I07=a.interchange_receiver_id, ISA_11_I65=a.interchange_control_standards_id, ISA_12_I11=a.interchange_control_version_no, ISA_13_I12=a.interchange_control_no_header, ISA_14_I13=a.acknowledge_requested, ISA_15_I14=a.usage_indicator, ISA_16_I15=a.component_separator, 
GS_01_479=a.functional_id_code, GS_02_142=a.application_sender_code, GS_03_124=a.application_receiver_code, 
GS_06_28=a.group_control_no_header, GS_07_455=a.responsible_agency_code, 
GS_08_480=a.version_code, GE_02_28=a.group_control_no_trailer, IEA_02_I12=a.interchange_control_no_trailer,       
ST_02_329=b.trans_set_control_no_header, ST_03_1705 = b.implementation_guide_version , BHT_02_353=b.trans_set_purpose_code, 
BHT_03_127=b.appl_trans_id, BHT_06_640=b.encounter_id, 
REF_02_127=b.trans_type_code, NM1_02_1065=b.submitter_entity_qualifier, 
NM1_03_1035_1000A=b.submitter_lname, NM1_04_1036=b.submitter_fname, NM1_05_1037=b.submitter_mname, 
NM1_09_67_1000A=b.submitter_id, PER_02_93=b.submitter_contact_name, PER_03_365=b.submitter_comm_no_1_qualifier, 
PER_04_364=b.submitter_comm_no_1, PER_05_365=b.submitter_comm_no_2_qualifier, PER_06_364=b.submitter_comm_no_2, 
PER_07_365=b.submitter_comm_no_3_qualifier, PER_08_364=b.submitter_comm_no_3, NM1_03_1035_1000B=b.receiver_lname, 
NM1_09_67_1000B=b.receiver_primary_id, 
SE_02_329=b.trans_set_control_no_trailer 
from HIPAA_Header_Trailer a, H837PI_Header_Trailer b  
where a.sp_id = b.sp_id
and a.sp_id = @sp_id

/* Begin 01/10/2011 Change


END     /* 5010 */ 
ELSE    /* IF @interchange_control_version_no = '00401' */ 
BEGIN
----------------------------------
-- this is the old 4010 code - can delete this whole branch when 4010 is obsolete
----------------------------------
select ISA_01_I01=a.authorization_id_qualifier, ISA_02_I02=a.authorization_id, ISA_03_I03=a.security_id_qualifier, ISA_04_I04=a.security_id, ISA_05_I05=a.interchange_sender_qualifier, ISA_06_I06=a.interchange_sender_id, ISA_07_I05=a.interchange_receiver_qualifier, ISA_08_I07=a.interchange_receiver_id, ISA_11_I10=a.interchange_control_standards_id, ISA_12_I11=a.interchange_control_version_no, ISA_13_I12=a.interchange_control_no_header, ISA_14_I13=a.acknowledge_requested, ISA_15_I14=a.usage_indicator, ISA_16_I15=a.component_separator, GS_01_479=a.functional_id_code, GS_02_142=a.application_sender_code, GS_03_124=a.application_receiver_code, GS_06_28=a.group_control_no_header, GS_07_455=a.responsible_agency_code, GS_08_480=a.version_code, GE_02_28=a.group_control_no_trailer, IEA_02_I12=a.interchange_control_no_trailer,       ST_02_329=b.trans_set_control_no_header, BHT_02_353=b.trans_set_purpose_code, BHT_03_127=b.appl_trans_id, BHT_06_640=b.encounter_id, REF_02_127=b.trans_type_code, NM1_02_1065=b.submitter_entity_qualifier, NM1_03_1035_1000A=b.submitter_lname, NM1_04_1036=b.submitter_fname, NM1_05_1037=b.submitter_mname, NM1_09_67_1000A=b.submitter_id, PER_02_93=b.submitter_contact_name, PER_03_365=b.submitter_comm_no_1_qualifier, PER_04_364=b.submitter_comm_no_1, PER_05_365=b.submitter_comm_no_2_qualifier, PER_06_364=b.submitter_comm_no_2, PER_07_365=b.submitter_comm_no_3_qualifier, PER_08_364=b.submitter_comm_no_3, NM1_03_1035_1000B=b.receiver_lname, NM1_09_67_1000B=b.receiver_primary_id, SE_02_329=b.trans_set_control_no_trailer   
from HIPAA_Header_Trailer a, H837PI_Header_Trailer b  
where a.sp_id = b.sp_id
and a.sp_id = @sp_id
END     /* 4010 */ 

End 01/10/2011 Change
*/


set NOCOUNT OFF
return 0

ERROR:
     Raiserror (@errmsg, 16, 1)
     return -1


go
grant execute on dbo.psp_h837pi_djs_header_trailer_5010 to secure
go
