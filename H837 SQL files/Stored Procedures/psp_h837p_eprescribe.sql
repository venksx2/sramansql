IF OBJECT_ID('dbo.psp_h837p_eprescribe', 'P') IS NOT NULL DROP PROCEDURE dbo.psp_h837p_eprescribe
GO


CREATE PROCEDURE dbo.psp_h837p_eprescribe
	@debug int = 0
AS
/***************************************************************************************************
* Stored Procedure: psp_h837p_eprescribe
* Creation Date:    05/07/2010
* Copyright:        2010 Askesis Development Group, Inc.
*
* Purpose: Add the service line required for the ePrescribe Incentive Program.
*
* Input Parameters:  @debug - print some debugging output if set to 1
* Output Parameters: N/A
*
* Return Status: return code is 0 on success
*
* Called By: psp_h837pi_shell
* Calls:     N/A
*
* Change Modification Log
*---------------------------------------------------------------------------------------------------
* Date        Author      Purpose
*---------------------------------------------------------------------------------------------------
* 05/07/2010  ParkBJ      #21140/#21248 - Created
* 05/20/2010  ParkBJ      #21140/#21248 - Spec change
* 05/27/2010  ParkBJ      #21140/#21248 - Spec change; service_line_counter bugfix
* 06/01/2010  NLEY        #21140/#21248 - Modified the service line counter calculation, renamed
* 06/09/2010  ParkBJ      #21140/#21248 - Modified the service line counter calculation (again)
* 06/23/2010  ParkBJ      #21140/#21248 - use unique, individual billing_trans_no (versus the group one)
* 07/12/2010  ParkBJ      #21140/#21248 - multiple changes
* 07/22/2010  ParkBJ      #21140/#21248 - fix to insert into eRX_Incentive_Program
***************************************************************************************************/
SET NOCOUNT ON

DECLARE	@spid int, @usage_indicator char(1)

DECLARE @eRX_claims table (
	claim_id varchar(10) NOT NULL, 
	billing_trans_no char(10) NULL, 
	line_item_control_no varchar(30) NULL,  -- added 07/12/2010
	service_line_counter_increment int NULL, 
	rownum int NOT NULL IDENTITY(1,1)
)


--SET @spid = dbo.psfn_get_spid (@debug)
SET @spid = @@spid

IF @debug = 1
BEGIN
	PRINT ' ****************************************************************** '	
	PRINT ' ***************** BEGIN ' + OBJECT_NAME(@@PROCID) + ' ******************** '
	PRINT ' ****************************************************************** '
	PRINT ''
	PRINT ' ------ Contents of H837P_Service at beginning of ' + OBJECT_NAME(@@PROCID) + ' ---------------- '
	SELECT * FROM dbo.H837P_Service WHERE sp_id = @spid ORDER BY claim_id, cast(service_line_counter as int), billing_trans_no
	PRINT ''
	PRINT ' ------ Contents of V_eRX_Incentive_Program at beginning of ' + OBJECT_NAME(@@PROCID) + ' ------ '
	SELECT * FROM dbo.V_eRX_Incentive_Program WHERE batch_id IN (select batch_id from #temp_bill_tran_list) ORDER BY billing_trans_no
	PRINT ''
	PRINT ' ------ Contents of #temp_bill_tran_list at beginning of ' + OBJECT_NAME(@@PROCID) + ' --------- '
	SELECT billing_trans_no, e_prescribe, claim_id FROM #temp_bill_tran_list ORDER BY billing_trans_no
	PRINT ''
END

IF NOT EXISTS (select * from #temp_coverage_plan where hipaa_code in ('MA', 'MB', 'MP')) BEGIN
	IF @debug = 1 PRINT 'Skipping logic in ' + OBJECT_NAME(@@PROCID) + ' because no applicable coverage plans have hipaa_code of MA/MB/MP.'
	GOTO end_proc
END


SELECT @usage_indicator = usage_indicator from #temp_arguments


---------------------------------------------------------------------------------------------------
-- Generate a new row in H837P_Service for each service for the current sp_id which meets the following conditions:
-- a. If any billing_trans_no matches a billing_trans_no in eRX_Incentive_Program
--  OR both of the following criteria are met:
-- 1. The services contains one or more billing transactions where #temp_bill_tran_list.e_prescribe = 'Y'. 
-- 2. There is not a row in eRX_Incentive_Program for the patient_id, episode_id, service_date and rendering_id.
--
-- Only one new row per claim can be added, but also grab the first billing_trans_no that has e_prescribe = 'Y'.

INSERT INTO @eRX_claims (claim_id, billing_trans_no, line_item_control_no)
SELECT DISTINCT qualifying.claim_id, qualifying.billing_trans_no, qualifying.line_item_control_no
FROM (  -- a.
	SELECT s.claim_id, /*s.billing_trans_no*/ btl.billing_trans_no, s.line_item_control_no  -- changed 06/23/2010
	FROM dbo.H837P_Service s 
	JOIN #temp_bill_tran_list btl ON btl.coll_billing_trans_no = s.billing_trans_no  -- added 06/23/2010
	JOIN dbo.V_eRX_Incentive_Program erx ON erx.billing_trans_no = /*s.billing_trans_no*/ btl.billing_trans_no  -- changed 06/23/2010
	WHERE s.sp_id = @spid
UNION   -- 1. and 2.
	SELECT s.claim_id, /*s.billing_trans_no*/ btl.billing_trans_no, s.line_item_control_no  -- changed 06/23/2010
	FROM dbo.H837P_Service s 
	JOIN #temp_bill_tran_list btl ON /*btl.billing_trans_no*/ btl.coll_billing_trans_no = s.billing_trans_no AND btl.e_prescribe = 'Y'  -- changed 06/23/2010
		AND NOT EXISTS (
			select * 
			from dbo.V_eRX_Incentive_Program erx 
			where erx.patient_id = btl.patient_id and erx.episode_id = btl.episode_id 
				and convert(varchar(10),erx.proc_chron,101) = convert(varchar(10),btl.proc_chron,101)  -- match on date, not date and time
				and erx.rendering_id = btl.billing_id
		)
	WHERE s.sp_id = @spid
) qualifying

-- ParkBJ (06/09/2010), NLEY (06/01/2010): @eRX_claims now holds one row for each new service line 
-- we need to add.  Here, for each claim_id, we sequentially number each line.  (Each claim_id will 
-- have numbering that starts at 1.)  We will need this later to assign the service_line_counter 
-- required on the 837.
UPDATE eRX
SET service_line_counter_increment = (
		select count(*) 
		from @eRX_claims eRX2 
		where eRX2.claim_id = eRX.claim_id and eRX2.rownum <= eRX.rownum )
FROM @eRX_claims eRX

IF @debug = 1 BEGIN
	PRINT ' ------ Contents of @eRX_claims ---------------- '
	SELECT * FROM @eRX_claims ORDER BY claim_id, billing_trans_no, line_item_control_no
END


---------------------------------------------------------------------------------------------------
--The new row in H837P_Service should be populated as follows:
-- a. sp_id = current sp_id.
-- b. claim_id = current claim_id.
-- c. billing_trans_no = concatenate a 'G' to the end of the billing transaction number of the 
--    first service encounted on the claim that is made up of at least one transaction that has 
--    e_prescribe = 'Y'.
-- d. service_line_counter = counter incremented by 1.
-- e. service_id_qualifier = HC.
-- f. proc_code = G8553.
-- g. line_item_charge_amt = 0.
-- h. unit_of_measure = 1.
-- i. diag_code_ptr_1 through diag_code_ptr_4 = the same values as those on the first service 
--    encounted on the claim that is made up of at least one transaction that has e_prescribe = 'Y'. 
-- j. service_date_qualifier = the same date qualifier as the one on the first service encounted 
--    on the claim that is made up of at least one transaction that has e_prescribe = 'Y'. 
-- k. service_date = the same date as the one on the first service encountered on the claim that 
--    is made up of at least one transaction that has e_prescribe = 'Y'. 
-- l. line_item_control_no = concatenate a 'G' to the end of the billing transaction number on the 
--    first service encounted on the claim that is made up of at least one transaction that has 
--    e_prescribe = 'Y'. 
INSERT INTO dbo.H837P_Service (sp_id, claim_id
	, billing_trans_no
	, service_line_counter
	, service_id_qualifier, proc_code, line_item_charge_amt, unit_of_measure, service_unit_count 
	, diag_code_ptr_1, diag_code_ptr_2, diag_code_ptr_3, diag_code_ptr_4
	, service_date_qualifier, service_date
	, line_item_control_no
)
SELECT @spid, erx.claim_id
	, rtrim(erx.billing_trans_no)+'G' as billing_trans_no
	, ( -- service_line_counter_increment is a sequential number starting at 1 for each claim_id; add it to the existing highest service_line_counter for the claim_id
		select max(cast(s2.service_line_counter as int)) + erx.service_line_counter_increment
		from dbo.H837P_Service s2 
		where s2.sp_id = @spid and s2.claim_id = erx.claim_id 
	) as service_line_counter
	, 'HC' as service_id_qualifier, 'G8553' as proc_code, 0.00 as line_item_charge_amt, 'UN' as unit_of_measure, '1' as service_unit_count
	, s.diag_code_ptr_1, s.diag_code_ptr_2, s.diag_code_ptr_3, s.diag_code_ptr_4
	, s.service_date_qualifier, s.service_date
	, rtrim(erx.billing_trans_no)+'G' as line_item_control_no
FROM @eRX_claims erx
JOIN dbo.H837P_Service s ON s.sp_id = @spid AND s.claim_id = erx.claim_id 
	--AND s.billing_trans_no = erx.billing_trans_no
	AND s.billing_trans_no = erx.line_item_control_no

IF @debug = 1 BEGIN
	PRINT ' ------ Contents of H837P_Service after inserting new row(s) ---------------- '
	SELECT * FROM dbo.H837P_Service WHERE sp_id = @spid ORDER BY claim_id, cast(service_line_counter as int), billing_trans_no
END


---------------------------------------------------------------------------------------------------
-- 9. Each time a new row is added into H83P_Service for the CPT code, G8553, insert a row into 
-- eRX_Incentive_Program for each individual billing transaction making up the claim that is not 
-- already in the table and where #temp_bill_tran_list.e_prescribe = 'Y'.
--
-- NOTE: When inserting into eRX_Incentive_Program, make sure only one row is being inserted.  
-- For instance, if the service is made up of three billing_transactions and all three have 
-- #temp_bill_tran_list.e_prescribe = 'Y', only one row not three should be entered.
IF @debug = 1 BEGIN
	PRINT 'Inserting rows into eRX_Incentive_Program'

	SELECT DISTINCT btl.billing_trans_no, btl.billing_id, btl.batch_id
	FROM @eRX_claims erx
	JOIN #temp_bill_tran_list btl ON btl.billing_trans_no = erx.billing_trans_no AND btl.e_prescribe = 'Y'
	JOIN dbo.H837P_Service s ON s.sp_id = @spid AND s.claim_id = erx.claim_id
		AND s.billing_trans_no = /*erx.billing_trans_no*/ erx.line_item_control_no  --07/22/2010
	WHERE NOT EXISTS (
		select * 
		from dbo.eRX_Incentive_Program erx2
		where erx2.billing_trans_no = s.billing_trans_no 
	)
END

INSERT INTO dbo.eRX_Incentive_Program (billing_trans_no, rendering_id, batch_id)
SELECT DISTINCT btl.billing_trans_no, btl.billing_id, btl.batch_id
FROM @eRX_claims erx
JOIN #temp_bill_tran_list btl ON btl.billing_trans_no = erx.billing_trans_no AND btl.e_prescribe = 'Y'
JOIN dbo.H837P_Service s ON s.sp_id = @spid AND s.claim_id = erx.claim_id
	AND s.billing_trans_no = /*erx.billing_trans_no*/ erx.line_item_control_no  --07/22/2010
WHERE NOT EXISTS (
	select * 
	from dbo.eRX_Incentive_Program erx2
	where erx2.billing_trans_no = s.billing_trans_no 
)


---------------------------------------------------------------------------------------------------
-- 10. If the 837 process is in test mode OR if the job is sent to the print queue, all the 
-- billing_trans_no for the current sp_id found in #temp_bill_tran_list where e_prescribe = 'Y', 
-- should be deleted out eRX_Incentive_Program.
IF @usage_indicator = 'T' or @debug = 1 BEGIN
	IF @debug = 1 BEGIN
		PRINT 'Deleting from eRX_Incentive_Program where e_prescribe = ''Y'' (always in debug mode)'
		PRINT '@usage_indicator = '''+@usage_indicator+''' (would always delete when ''T'')'

		SELECT btl.e_prescribe, erx.*
		FROM dbo.eRX_Incentive_Program erx 
		JOIN #temp_bill_tran_list btl ON btl.e_prescribe = 'Y' 
			AND /*btl.billing_trans_no*/ btl.coll_billing_trans_no = erx.billing_trans_no -- 06/23/2010
			AND btl.batch_id = erx.batch_id
	END

	DELETE erx 
	FROM dbo.eRX_Incentive_Program erx 
	JOIN #temp_bill_tran_list btl ON btl.e_prescribe = 'Y' 
		AND /*btl.billing_trans_no*/ btl.coll_billing_trans_no = erx.billing_trans_no -- 06/23/2010
		AND erx.batch_id = btl.batch_id
END ELSE IF EXISTS (select * from #temp_error) BEGIN
	-- (These two deletions are based on logic found in psp_h837pi_shell.)
	--
	-- Here we will catch what we know is going to the print queue because of errors, but if in the 
	-- GUI the user deselects "Did this print correctly?" after the process has run, we will have to 
	-- use psp_delete_claim_batch (called directly by GUI) to clean up.
	IF @debug = 1 BEGIN
		PRINT 'Deleting from eRX_Incentive_Program where e_prescribe = ''Y'' and #temp_error exists (job is sent to print queue)'
		
		SELECT btl.e_prescribe, err.unique_id as "#temp_error.unique_id", erx.*
		FROM dbo.eRX_Incentive_Program erx 
		JOIN #temp_bill_tran_list btl ON btl.e_prescribe = 'Y' 
			AND /*btl.billing_trans_no*/ btl.coll_billing_trans_no = erx.billing_trans_no -- 06/23/2010
			AND btl.batch_id = erx.batch_id
		JOIN #temp_error err ON (err.unique_id = btl.billing_trans_no AND err.label = 'BILLING_TRANS_NO')
	END

	DELETE erx 
	FROM dbo.eRX_Incentive_Program erx 
	JOIN #temp_bill_tran_list btl ON btl.e_prescribe = 'Y' 
		AND /*btl.billing_trans_no*/ btl.coll_billing_trans_no = erx.billing_trans_no -- 06/23/2010
		AND btl.batch_id = erx.batch_id
	JOIN #temp_error err ON (err.unique_id = btl.billing_trans_no AND err.label = 'BILLING_TRANS_NO')

	IF @debug = 1 BEGIN
		PRINT 'If not in debug mode, would delete from eRX_Incentive_Program where e_prescribe = ''Y'' and is_837_printed = ''N'' (job is sent to print queue)'
		
		SELECT btl.e_prescribe, btl.is_837_printed, erx.*
		FROM dbo.eRX_Incentive_Program erx 
		JOIN #temp_bill_tran_list btl ON btl.e_prescribe = 'Y' 
			AND /*btl.billing_trans_no*/ btl.coll_billing_trans_no = erx.billing_trans_no -- 06/23/2010
			AND btl.is_837_printed = 'N'
			AND btl.batch_id = erx.batch_id
		JOIN #temp_error err ON (err.unique_id = btl.billing_trans_no AND err.label = 'BILLING_TRANS_NO')
	END

	IF @debug = 0
		DELETE erx 
		FROM dbo.eRX_Incentive_Program erx 
		JOIN #temp_bill_tran_list btl ON btl.e_prescribe = 'Y' 
			AND /*btl.billing_trans_no*/ btl.coll_billing_trans_no = erx.billing_trans_no -- 06/23/2010
			AND btl.is_837_printed = 'N'
			AND btl.batch_id = erx.batch_id
END


---------------------------------------------------------------------------------------------------
-- 11. If a billing transaction throws a validation error and #temp_bill_tran_list.e_prescribe = 'Y', 
-- the row in the eRX_Incentive_Program corresponding to that billing_trans_no and sp_id should 
-- also be deleted.
-- 
-- This should possibly be moved to psp_h837pi_delete, but that is only called (in psp_h837pi_shell, 
-- after running everything else) if @usage_indicator = 'P' or @debug = 1.
DELETE erx 
FROM #temp_error err 
JOIN #temp_bill_tran_list btl ON btl.billing_trans_no = err.billing_trans_no AND btl.e_prescribe = 'Y'
JOIN dbo.eRX_Incentive_Program erx ON erx.billing_trans_no = /*btl.billing_trans_no*/ btl.coll_billing_trans_no -- 06/23/2010
	AND erx.batch_id = btl.batch_id


---------------------------------------------------------------------------------------------------
end_proc:

IF @debug = 1
BEGIN
	PRINT ''
	PRINT ' ------ Contents of @eRX_claims on completion of ' + OBJECT_NAME(@@PROCID) + ' -------- '
	SELECT * FROM @eRX_claims ORDER BY claim_id
	PRINT ''
	PRINT ' ------ Contents of H837P_Service on completion of ' + OBJECT_NAME(@@PROCID) + ' ------ '
	SELECT * FROM dbo.H837P_Service WHERE sp_id = @spid ORDER BY claim_id, cast(service_line_counter as int), billing_trans_no
	PRINT ''
	PRINT ' ****************************************************************** '	
	PRINT ' ****************** END ' + OBJECT_NAME(@@PROCID) + ' ********************* '
	PRINT ' ****************************************************************** '
	PRINT ''
END

RETURN 0
GO

GRANT EXECUTE ON dbo.psp_h837p_eprescribe TO secure
GO
