if exists (select * from sysobjects where id = object_id('dbo.psp_h837pi_cleanup'))
		drop proc dbo.psp_h837pi_cleanup
go

create procedure dbo.psp_h837pi_cleanup
			@spid			integer	= NULL,
			@check_psych_config 	int = 0,
			@debug			int	= 0
as

set nocount on

/****************************************************************************************************************
 Stored Procedure:	psp_h837pi_cleanup
 Creation Date:		11/07/02
 Copyright:		2002 Askesis Development Group, Inc.
 Purpose:		Populates H837PI_BillingPayToProvider table.
 
 Input Parameters:
 Output Parameters:
 Return Status:
 Called By:		I don't know right now ...
 Calls:			I don't know right now ...

 Change Modification Log
-----------------------------------------------------------------------------------------------------------------------
 Author		Date		Ver		Purpose (note Onyx QA # if relevant)                              
-----------------------------------------------------------------------------------------------------------------------
 Srini		11/07/02	1	#13775-- Cleans all the tables that are read by data junction
					to generate EDI files. This cleanup is based on @@spid.
 Raghu		12/22/02	2	Added the @check_psych_config argument.
 BPillai	08/01/03	3	Added cleaning up of Institution Tables too.
**********************************************************************************************************************/

-- If in test mode, then do not cleanup the Intermediate tables. This is used for Debugging purporses. The client SP call always passes a 1 for @check_psych_config.
if @check_psych_config = 1 and exists (select * from Psych_Config where code = '837USAGE_IND' and value like 'T%')
  return 0

if @spid is null 
	select @spid = @@spid

begin tran 


delete from dbo.H837PI_Adjustment where sp_id = @spid
delete from dbo.H837PI_SVD_DTP where sp_id = @spid
delete from dbo.H837P_Service where sp_id = @spid
delete from dbo.H837P_Claim_Payor where sp_id = @spid
delete from dbo.H837P_Claim where sp_id = @spid

/* BPillai	08/01/03	3 Added for Institutional */
delete from dbo.H837I_Service where sp_id = @spid
delete from dbo.H837I_Claim_Payor where sp_id = @spid
delete from dbo.H837I_Claim where sp_id = @spid      

delete from dbo.H837PI_Subscriber_Patient where sp_id = @spid
delete from dbo.H837PI_BillingPayToProvider where sp_id = @spid
delete from dbo.H837PI_Header_Trailer where sp_id = @spid
delete from dbo.HIPAA_Header_Trailer where sp_id = @spid				

commit tran 

set nocount off

return 0
go

grant execute on dbo.psp_h837pi_cleanup to secure
go
